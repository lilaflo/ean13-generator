<?php

namespace App;

use Picqer\Barcode\BarcodeGeneratorSVG;
use TCPDF;

require __DIR__ . '/../vendor/autoload.php';

class Generator
{

    private $tmpFileName;
    private $generator;
    private $height = 30;
    private $width = 50;
    private $fontFamily = 'Times';
    private $fontSize = 9;
    private $pageSize = [];

    private $creator = 'wantanda UG (haftungsbeschränkt)';

    public function __construct()
    {
        $this->generator = new BarcodeGeneratorSVG();
        $this->pageSize = [$this->width, $this->height];
    }

    public function generateBarcodePdf($code): string
    {
        $cleanNumber = $this->cleanNumber($code);
        $splitNumber = $this->splitNumber($cleanNumber);
        $barcode = $this->generateBarcode($cleanNumber);

        $pdf = new TCPDF('L', 'mm');
        $pdf->SetFont($this->fontFamily, '', $this->fontSize);
        $pdf->SetMargins(3, 3, 3);
        $pdf->SetCreator($this->creator);
        $pdf->SetAutoPageBreak(false);
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        $pdf->AddPage('L', $this->pageSize);
        $pdf->Cell(0, 44, 'ISBN ' . implode('  ', $splitNumber), 0, 0, 'C');
        $pdf->ImageSVG($barcode, 0, 3, $this->width, $this->height - 10);

        return $pdf->getPDFData();
    }

    public function cleanNumber($number): int
    {
        return (int)preg_replace('@\D+@', '', $number);
    }

    private function splitNumber(int $number): array
    {
        preg_match('@^(\d)(\d{6})(\d{6})$@', $number, $match);
        array_shift($match);

        return $match;
    }

    private function generateBarcode(int $eanCode): string
    {
        if (!file_put_contents(
            $this->getTemporaryFileName(),
            $this->generator->getBarcode($this->cleanNumber($eanCode), $this->generator::TYPE_EAN_13, 4, 200))
        ) {
            throw new \Exception('Unable to generate temporary file');
        }

        return $this->getTemporaryFileName();
    }

    private function getTemporaryFileName(): string
    {
        if (null === $this->tmpFileName) {
            $this->tmpFileName = tempnam(sys_get_temp_dir(), 'mottobox-ean13') . '.svg';
        }

        return $this->tmpFileName;
    }

    public function __destruct()
    {
        if (file_exists($this->getTemporaryFileName())) {
            unlink($this->getTemporaryFileName());
            unset($this->tmpFileName);
        }
    }
}