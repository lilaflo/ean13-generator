EAN-PDF generator
=================

This script generates a printable EAN sticker out of an ean code.

Example
-------

php generate.php 978-3-947299-00-3 

**or to generate multiple EAN codes:**

php generate.php 978-3-947299-00-3 978-3-947299-06-5

To convert a font to be usable with FPDF
----------------------------------------
Use the converter here: http://fpdf.fruit-lab.de/index.php
