with import <nixpkgs> {};

stdenv.mkDerivation rec {
    name = "EanGenerator";

    buildInputs = [
      php71
      php71Packages.composer
    ];

    shellHooks = ''
      composer install
    '';
}
