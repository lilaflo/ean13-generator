<?php

use App\Generator;

require __DIR__ . '/vendor/autoload.php';

$arguments = $_SERVER['argv'];

if (2 > count($arguments)) {
    throw new Exception('No code given');
}

array_shift($arguments);

foreach ($arguments as $argument) {
    $pdf = new Generator();
    file_put_contents(
        __DIR__ . DIRECTORY_SEPARATOR . $pdf->cleanNumber($argument) . '.pdf',
        $pdf->generateBarcodePdf($argument)
    );
}

